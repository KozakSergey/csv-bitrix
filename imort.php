<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

// ID информационного блока и раздела
$iblockID = 9;
$sectionID = 212;

// Открываем CSV-файл для записи
$csvFile = fopen($_SERVER["DOCUMENT_ROOT"] . "/exported_data.csv", "w");

// Заголовок CSV-файла
fputcsv($csvFile, array("ID", "Название", "Описание", "Изображение", "Свойство 1", "Свойство 2", /*...*/));

// Получаем элементы из раздела
$arFilter = array("IBLOCK_ID" => $iblockID, "SECTION_ID" => $sectionID);
$arSelect = array("ID", "NAME", "DETAIL_TEXT");
$arOrder = array("ID" => "ASC");
$res = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);

while ($arItem = $res->GetNext()) {
    // Получаем свойства элемента
    $arProps = CIBlockElement::GetProperty($iblockID, $arItem["ID"]);
    
    $row = array(
        $arItem["ID"],
        $arItem["NAME"],
        $arItem["DETAIL_TEXT"],
    );
    
    // Добавляем изображение (предположим, что оно есть в свойстве PREVIEW_PICTURE)
    if ($arItem["PREVIEW_PICTURE"]) {
        $row[] = "http://ваш_сайт" . CFile::GetPath($arItem["PREVIEW_PICTURE"]);
    } else {
        $row[] = "";
    }

    // Добавляем свойства
    while ($prop = $arProps->Fetch()) {
        $row[] = $prop["VALUE"];
    }

    // Записываем строку в CSV-файл
    fputcsv($csvFile, $row);
}

// Закрываем CSV-файл
fclose($csvFile);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>